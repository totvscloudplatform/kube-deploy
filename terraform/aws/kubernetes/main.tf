variable region {}
variable public_domain {}
variable cluster_name {}
variable k8s_version {}
variable kubeadm_token {}
variable assume_role_arn {}
variable zone {} # a,b or c
variable public_key {}

variable instance_type {
  default = "c5.4xlarge"
}

provider "aws" {
  version = "~> 1.13.0"
  region  = "${var.region}"

  assume_role {
    role_arn     = "${var.assume_role_arn}"
    session_name = "k8s-bootstrap"
  }
}

# Alias for service account id 884267208100 to create DNS delegation
provider "aws" {
  alias  = "service"
  region = "us-east-2"
}

# mapping common tags for k8s.
# Issue https://github.com/hashicorp/terraform/issues/14516
locals {
  common_tags = "${map(
    "kubernetes.io/cluster/${var.cluster_name}.${var.public_domain}", "shared"
  )}"

  autoscaling_common_tags = "${map(
    "key", "kubernetes.io/cluster/${var.cluster_name}.${var.public_domain}", 
    "value", "shared",
    "propagate_at_launch", true
  )}"
}

data "aws_subnet" "current-subnet" {
  depends_on = ["aws_subnet.nodes-private-zone-a", "aws_subnet.nodes-private-zone-b", "aws_subnet.nodes-private-zone-c"]

  filter {
    name   = "tag:Name"
    values = ["nodes.${var.region}${var.zone}.${var.cluster_name}.${var.public_domain}"]
  }
}

data "aws_ami" "k8s-ami" {
  most_recent = true

  filter {
    name = "tag:KubernetesVersion"

    # This version will be used in the Kubernetes control plane images
    # https://storage.googleapis.com/kubernetes-release
    # Example: https://storage.googleapis.com/kubernetes-release/release/stable.txt
    values = ["${var.k8s_version}"]
  }
}

data "aws_ami" "bastion-ami" {
  most_recent = true
  owners      = ["aws-marketplace"]

  filter {
    name   = "product-code"
    values = ["aw0evgkw8e5c1q413zgy5pjce"]
  }
}

data "aws_route53_zone" "principal_zone" {
  provider     = "aws.service"
  name         = "${var.public_domain}"
  private_zone = false
}

data "template_file" "nodes" {
  template = "${file("${path.module}/data/aws_launch_configuration_node_kubeadm-join.tpl")}"

  vars {
    internal_master_address = "api-internal.${var.cluster_name}.${var.public_domain}"
    kubeadm_token           = "${var.kubeadm_token}"
  }
}

data "template_file" "masters" {
  template = "${file("${path.module}/data/aws_launch_configuration_master_kubeadm-init.tpl")}"

  vars {
    kubeadm_config           = "/home/centos/master.yaml"
    internal_master_address  = "api-internal.${var.cluster_name}.${var.public_domain}"
    external_master_address  = "api.${var.cluster_name}.${var.public_domain}"
    efs_dns                  = "${aws_efs_file_system.etcd.dns_name}"
    kubeadm_token            = "${var.kubeadm_token}"
    k8s_version              = "${var.k8s_version}"
    controller_efs01_id      = "${aws_efs_file_system.efs01-controller.id}"
    controller_efs01_address = "${aws_efs_file_system.efs01-controller.dns_name}"
  }
}

# RDS subnet group for applications
resource "aws_db_subnet_group" "nodes-to-rds" {
  name       = "private"
  subnet_ids = ["${aws_subnet.nodes-private-zone-a.id}", "${aws_subnet.nodes-private-zone-b.id}", "${aws_subnet.nodes-private-zone-c.id}"]

  tags {
    Name = "private.${var.cluster_name}.${var.public_domain}"
  }
}

# EFS for applications
resource "aws_efs_file_system" "efs01-controller" {
  creation_token = "efs01-controller-${var.cluster_name}"

  tags {
    Name = "efs01-controller-${var.cluster_name}"
  }
}

resource "aws_efs_mount_target" "nodes-zone-a" {
  file_system_id  = "${aws_efs_file_system.efs01-controller.id}"
  subnet_id       = "${aws_subnet.nodes-private-zone-a.id}"
  security_groups = ["${aws_security_group.efs-controller.id}"]
}

resource "aws_efs_mount_target" "nodes-zone-b" {
  file_system_id  = "${aws_efs_file_system.efs01-controller.id}"
  subnet_id       = "${aws_subnet.nodes-private-zone-b.id}"
  security_groups = ["${aws_security_group.efs-controller.id}"]
}

resource "aws_efs_mount_target" "nodes-zone-c" {
  file_system_id  = "${aws_efs_file_system.efs01-controller.id}"
  subnet_id       = "${aws_subnet.nodes-private-zone-c.id}"
  security_groups = ["${aws_security_group.efs-controller.id}"]
}

resource "aws_route53_zone" "public_zone" {
  name = "${var.cluster_name}.${var.public_domain}"
}

resource "aws_route53_record" "ns" {
  provider = "aws.service"
  zone_id  = "${data.aws_route53_zone.principal_zone.zone_id}"
  name     = "${var.cluster_name}.${var.public_domain}"
  type     = "NS"
  ttl      = "30"

  records = [
    "${aws_route53_zone.public_zone.name_servers.0}",
    "${aws_route53_zone.public_zone.name_servers.1}",
    "${aws_route53_zone.public_zone.name_servers.2}",
    "${aws_route53_zone.public_zone.name_servers.3}",
  ]
}

resource "aws_acm_certificate" "cert" {
  domain_name       = "*.${var.cluster_name}.${var.public_domain}"
  validation_method = "DNS"

  tags {
    Environment = "production"
  }
}

resource "aws_route53_record" "cert_validation" {
  name    = "${aws_acm_certificate.cert.domain_validation_options.0.resource_record_name}"
  type    = "${aws_acm_certificate.cert.domain_validation_options.0.resource_record_type}"
  zone_id = "${aws_route53_zone.public_zone.id}"
  records = ["${aws_acm_certificate.cert.domain_validation_options.0.resource_record_value}"]
  ttl     = 60
}

resource "aws_acm_certificate_validation" "cert" {
  certificate_arn         = "${aws_acm_certificate.cert.arn}"
  validation_record_fqdns = ["${aws_route53_record.cert_validation.fqdn}"]
}

resource "aws_route53_record" "master-external" {
  zone_id = "${aws_route53_zone.public_zone.zone_id}"
  name    = "api"
  type    = "A"

  alias {
    name                   = "${aws_lb.master-external.dns_name}"
    zone_id                = "${aws_lb.master-external.zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "master-internal" {
  zone_id = "${aws_route53_zone.public_zone.zone_id}"
  name    = "api-internal"
  type    = "A"

  alias {
    name                   = "${aws_lb.master-internal.dns_name}"
    zone_id                = "${aws_lb.master-internal.zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "bastion" {
  zone_id = "${aws_route53_zone.public_zone.zone_id}"
  name    = "bastion"
  type    = "A"

  alias {
    name                   = "${aws_lb.bastion.dns_name}"
    zone_id                = "${aws_lb.bastion.zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_efs_file_system" "etcd" {
  creation_token = "master-etcd-${var.cluster_name}"

  tags {
    Name = "master-etcd-${var.cluster_name}"
  }
}

resource "aws_efs_mount_target" "masters-zone-a" {
  file_system_id  = "${aws_efs_file_system.etcd.id}"
  subnet_id       = "${aws_subnet.master-private-zone-a.id}"
  security_groups = ["${aws_security_group.master-efs.id}"]
}

resource "aws_efs_mount_target" "masters-zone-b" {
  file_system_id  = "${aws_efs_file_system.etcd.id}"
  subnet_id       = "${aws_subnet.master-private-zone-b.id}"
  security_groups = ["${aws_security_group.master-efs.id}"]
}

resource "aws_efs_mount_target" "masters-zone-c" {
  file_system_id  = "${aws_efs_file_system.etcd.id}"
  subnet_id       = "${aws_subnet.master-private-zone-c.id}"
  security_groups = ["${aws_security_group.master-efs.id}"]
}

resource "aws_autoscaling_attachment" "master-external" {
  alb_target_group_arn   = "${aws_lb_target_group.master-external.arn}"
  autoscaling_group_name = "${aws_autoscaling_group.master.name}"
}

resource "aws_autoscaling_attachment" "master-internal" {
  alb_target_group_arn   = "${aws_lb_target_group.master-internal.arn}"
  autoscaling_group_name = "${aws_autoscaling_group.master.name}"
}

resource "aws_autoscaling_attachment" "bastion" {
  alb_target_group_arn   = "${aws_lb_target_group.bastion.arn}"
  autoscaling_group_name = "${aws_autoscaling_group.bastion.name}"
}

resource "aws_autoscaling_group" "bastion" {
  lifecycle {
    create_before_destroy = true
  }

  name                 = "bastions-${var.cluster_name}-${aws_launch_configuration.bastions.id}"
  launch_configuration = "${aws_launch_configuration.bastions.id}"
  max_size             = 1
  min_size             = 1
  vpc_zone_identifier  = ["${data.aws_subnet.current-subnet.id}"]

  tags = {
    key                 = "Name"
    value               = "bastion.${var.cluster_name}.${var.public_domain}"
    propagate_at_launch = true
  }
}

resource "aws_autoscaling_group" "master" {
  lifecycle {
    create_before_destroy = true
  }

  name                 = "master-${var.cluster_name}-${aws_launch_configuration.master.id}"
  launch_configuration = "${aws_launch_configuration.master.id}"
  max_size             = 1
  min_size             = 1
  desired_capacity     = 1
  vpc_zone_identifier  = ["${aws_subnet.master-private-zone-a.id}", "${aws_subnet.master-private-zone-b.id}", "${aws_subnet.master-private-zone-c.id}"]

  tags = ["${merge(
    local.autoscaling_common_tags
  )}"]

  tags = [
    {
      key                 = "Name"
      value               = "master.${var.cluster_name}.${var.public_domain}"
      propagate_at_launch = true
    },
  ]
}

resource "aws_autoscaling_group" "nodes" {
  lifecycle {
    create_before_destroy = true
  }

  depends_on           = ["aws_subnet.nodes-private-zone-a", "aws_subnet.nodes-private-zone-b", "aws_subnet.nodes-private-zone-c"]
  name                 = "nodes-${var.cluster_name}-${aws_launch_configuration.nodes.id}"
  launch_configuration = "${aws_launch_configuration.nodes.id}"
  max_size             = 50
  min_size             = 25
  desired_capacity     = 25

  vpc_zone_identifier = ["${data.aws_subnet.current-subnet.id}"]

  tags = ["${merge(
    local.autoscaling_common_tags
  )}"]

  tags = [
    {
      key                 = "Name"
      value               = "nodes.${var.cluster_name}.${var.public_domain}"
      propagate_at_launch = true
    },
  ]
}

resource "aws_lb_target_group" "master-external" {
  name     = "master-external-${var.cluster_name}"
  port     = 6443
  protocol = "TCP"
  vpc_id   = "${aws_vpc.vpc.id}"

  health_check {
    protocol            = "TCP"
    interval            = 10
    healthy_threshold   = 2
    unhealthy_threshold = 2
  }
}

resource "aws_lb_target_group" "master-internal" {
  name     = "master-internal-${var.cluster_name}"
  port     = 6443
  protocol = "TCP"
  vpc_id   = "${aws_vpc.vpc.id}"

  health_check {
    protocol            = "TCP"
    interval            = 10
    healthy_threshold   = 2
    unhealthy_threshold = 2
  }
}

resource "aws_lb_target_group" "bastion" {
  name     = "bastion-${var.cluster_name}"
  port     = 22
  protocol = "TCP"
  vpc_id   = "${aws_vpc.vpc.id}"

  health_check {
    protocol            = "TCP"
    interval            = 30
    healthy_threshold   = 2
    unhealthy_threshold = 2
  }
}

resource "aws_lb_listener" "master-external" {
  load_balancer_arn = "${aws_lb.master-external.arn}"
  port              = 443
  protocol          = "TCP"

  default_action {
    target_group_arn = "${aws_lb_target_group.master-external.arn}"
    type             = "forward"
  }
}

resource "aws_lb_listener" "master-internal" {
  load_balancer_arn = "${aws_lb.master-internal.arn}"
  port              = 6443
  protocol          = "TCP"

  default_action {
    target_group_arn = "${aws_lb_target_group.master-internal.arn}"
    type             = "forward"
  }
}

resource "aws_lb_listener" "bastion" {
  load_balancer_arn = "${aws_lb.bastion.arn}"
  port              = 22
  protocol          = "TCP"

  default_action {
    target_group_arn = "${aws_lb_target_group.bastion.arn}"
    type             = "forward"
  }
}

resource "aws_lb" "bastion" {
  name                             = "bastion-${var.cluster_name}"
  subnets                          = ["${aws_subnet.public-zone-a.id}", "${aws_subnet.public-zone-b.id}", "${aws_subnet.public-zone-c.id}"]
  internal                         = false
  idle_timeout                     = 1200
  load_balancer_type               = "network"
  enable_cross_zone_load_balancing = true

  tags {
    Name = "bastion.${var.cluster_name}.${var.public_domain}"
    Role = "public"
  }
}

resource "aws_lb" "master-external" {
  name                             = "master-external-${var.cluster_name}"
  subnets                          = ["${aws_subnet.public-zone-a.id}", "${aws_subnet.public-zone-b.id}", "${aws_subnet.public-zone-c.id}"]
  internal                         = false
  idle_timeout                     = 300
  load_balancer_type               = "network"
  enable_cross_zone_load_balancing = false

  tags {
    Name = "master-external.${var.cluster_name}.${var.public_domain}"
    Role = "public"
  }
}

resource "aws_lb" "master-internal" {
  name                             = "master-internal-${var.cluster_name}"
  subnets                          = ["${aws_subnet.master-private-zone-a.id}", "${aws_subnet.master-private-zone-b.id}", "${aws_subnet.master-private-zone-c.id}"]
  internal                         = true
  idle_timeout                     = 300
  load_balancer_type               = "network"
  enable_cross_zone_load_balancing = false

  tags {
    Name = "master-internal.${var.cluster_name}.${var.public_domain}"
    Role = "private"
  }
}

resource "aws_iam_instance_profile" "bastions" {
  name = "bastions.${var.cluster_name}.${var.public_domain}"
  role = "${aws_iam_role.bastions.name}"
}

resource "aws_iam_instance_profile" "masters" {
  name = "masters.${var.cluster_name}.${var.public_domain}"
  role = "${aws_iam_role.masters.name}"
}

resource "aws_iam_instance_profile" "nodes" {
  name = "nodes.${var.cluster_name}.${var.public_domain}"
  role = "${aws_iam_role.nodes.name}"
}

resource "aws_iam_role" "bastions" {
  name               = "bastions.${var.cluster_name}.${var.public_domain}"
  assume_role_policy = "${file("${path.module}/data/aws_iam_role_bastions_policy")}"
}

resource "aws_iam_role" "masters" {
  name               = "masters.${var.cluster_name}.${var.public_domain}"
  assume_role_policy = "${file("${path.module}/data/aws_iam_role_masters_policy")}"
}

resource "aws_iam_role" "nodes" {
  name               = "nodes.${var.cluster_name}.${var.public_domain}"
  assume_role_policy = "${file("${path.module}/data/aws_iam_role_nodes_policy")}"
}

resource "aws_iam_role" "rdsEnhancedMonitoringRole" {
  name               = "rdsEnhancedMonitoringRole"
  assume_role_policy = "${file("${path.module}/data/aws_iam_role_rdsEnhancedMonitoringRole_policy")}"
}

resource "aws_iam_role_policy_attachment" "rdsEnhancedMonitoringRole" {
  role       = "${aws_iam_role.rdsEnhancedMonitoringRole.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonRDSEnhancedMonitoringRole"
}

resource "aws_iam_role_policy" "bastions" {
  name   = "bastions.${var.cluster_name}.${var.public_domain}"
  role   = "${aws_iam_role.bastions.name}"
  policy = "${file("${path.module}/data/aws_iam_role_policy_bastions_policy")}"
}

resource "aws_iam_role_policy" "masters" {
  name   = "masters.${var.cluster_name}.${var.public_domain}"
  role   = "${aws_iam_role.masters.name}"
  policy = "${file("${path.module}/data/aws_iam_role_policy_masters_policy")}"
}

resource "aws_iam_role_policy" "nodes" {
  name   = "nodes.${var.cluster_name}.${var.public_domain}"
  role   = "${aws_iam_role.nodes.name}"
  policy = "${file("${path.module}/data/aws_iam_role_policy_nodes_policy")}"
}

resource "aws_key_pair" "kubernetes" {
  key_name   = "kubernetes-${var.cluster_name}"
  public_key = "${var.public_key}"
}

resource "aws_launch_configuration" "bastions" {
  name_prefix                 = "bastions-"
  image_id                    = "${data.aws_ami.bastion-ami.id}"
  instance_type               = "${var.instance_type}"
  key_name                    = "${aws_key_pair.kubernetes.id}"
  iam_instance_profile        = "${aws_iam_instance_profile.bastions.id}"
  security_groups             = ["${aws_security_group.bastion.id}"]
  associate_public_ip_address = true
  user_data                   = "${file("${path.module}/data/aws_launch_configuration_bastion_user_data")}"

  root_block_device = {
    volume_type           = "gp2"
    volume_size           = 32
    delete_on_termination = true
  }

  lifecycle = {
    create_before_destroy = true
  }
}

resource "aws_launch_configuration" "master" {
  name_prefix                 = "master-${var.cluster_name}.${var.public_domain}-"
  image_id                    = "${data.aws_ami.k8s-ami.image_id}"
  instance_type               = "${var.instance_type}"
  key_name                    = "${aws_key_pair.kubernetes.id}"
  iam_instance_profile        = "${aws_iam_instance_profile.masters.id}"
  security_groups             = ["${aws_security_group.masters.id}"]
  associate_public_ip_address = false
  user_data                   = "${data.template_file.masters.rendered}"

  root_block_device = {
    volume_type           = "gp2"
    volume_size           = 100
    delete_on_termination = true
  }

  lifecycle = {
    create_before_destroy = true
  }
}

resource "aws_launch_configuration" "nodes" {
  name_prefix                 = "nodes.-"
  image_id                    = "${data.aws_ami.k8s-ami.image_id}"
  instance_type               = "${var.instance_type}"
  key_name                    = "${aws_key_pair.kubernetes.id}"
  iam_instance_profile        = "${aws_iam_instance_profile.nodes.id}"
  security_groups             = ["${aws_security_group.nodes.id}"]
  associate_public_ip_address = false
  user_data                   = "${data.template_file.nodes.rendered}"

  root_block_device = {
    volume_type           = "gp2"
    volume_size           = 500
    delete_on_termination = true
  }

  lifecycle = {
    create_before_destroy = true
  }
}

resource "aws_security_group_rule" "all-master-to-master" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters.id}"
  source_security_group_id = "${aws_security_group.masters.id}"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
}

resource "aws_security_group_rule" "all-master-to-node" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.nodes.id}"
  source_security_group_id = "${aws_security_group.masters.id}"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
}

resource "aws_security_group_rule" "all-node-to-node" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.nodes.id}"
  source_security_group_id = "${aws_security_group.nodes.id}"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
}

resource "aws_security_group_rule" "bastion-egress" {
  type              = "egress"
  security_group_id = "${aws_security_group.bastion.id}"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "bastion-to-master-ssh" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters.id}"
  source_security_group_id = "${aws_security_group.bastion.id}"
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
}

resource "aws_security_group_rule" "bastion-to-node-ssh" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.nodes.id}"
  source_security_group_id = "${aws_security_group.bastion.id}"
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
}

resource "aws_security_group_rule" "https-nlb-to-master-external" {
  type              = "ingress"
  security_group_id = "${aws_security_group.masters.id}"
  from_port         = 6443
  to_port           = 6443
  protocol          = "tcp"
  cidr_blocks       = ["138.219.88.0/24", "13.58.221.89/32", "18.220.12.3/32", "18.221.117.151/32", "52.14.200.118/32", "18.216.213.165/32", "18.221.186.110/32", "18.216.226.230/32"]
}

resource "aws_security_group_rule" "https-nlb-to-master-internal" {
  type              = "ingress"
  security_group_id = "${aws_security_group.masters.id}"
  from_port         = 6443
  to_port           = 6443
  protocol          = "tcp"
  cidr_blocks       = ["${aws_vpc.vpc.cidr_block}"]
}

resource "aws_security_group_rule" "master-egress" {
  type              = "egress"
  security_group_id = "${aws_security_group.masters.id}"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "node-egress" {
  type              = "egress"
  security_group_id = "${aws_security_group.nodes.id}"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "node-to-master-protocol-ipip" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters.id}"
  source_security_group_id = "${aws_security_group.nodes.id}"
  from_port                = 0
  to_port                  = 65535
  protocol                 = "4"
}

resource "aws_security_group_rule" "node-to-master-tcp-1-2379" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters.id}"
  source_security_group_id = "${aws_security_group.nodes.id}"
  from_port                = 1
  to_port                  = 2379
  protocol                 = "tcp"
}

resource "aws_security_group_rule" "node-to-master-tcp-2382-4001" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters.id}"
  source_security_group_id = "${aws_security_group.nodes.id}"
  from_port                = 2382
  to_port                  = 4001
  protocol                 = "tcp"
}

resource "aws_security_group_rule" "node-to-master-tcp-4003-65535" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters.id}"
  source_security_group_id = "${aws_security_group.nodes.id}"
  from_port                = 4003
  to_port                  = 65535
  protocol                 = "tcp"
}

resource "aws_security_group_rule" "node-to-master-udp-1-65535" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters.id}"
  source_security_group_id = "${aws_security_group.nodes.id}"
  from_port                = 1
  to_port                  = 65535
  protocol                 = "udp"
}

resource "aws_security_group_rule" "ssh-external-to-bastion-0-0-0-0--0" {
  type              = "ingress"
  security_group_id = "${aws_security_group.bastion.id}"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["18.188.212.34/32"]
}

resource "aws_security_group_rule" "master-to-efs" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.master-efs.id}"
  source_security_group_id = "${aws_security_group.masters.id}"
  from_port                = 2049
  to_port                  = 2049
  protocol                 = "tcp"
}

resource "aws_security_group_rule" "nodes-to-efs-controller" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.efs-controller.id}"
  source_security_group_id = "${aws_security_group.nodes.id}"
  from_port                = 2049
  to_port                  = 2049
  protocol                 = "tcp"
}

resource "aws_security_group_rule" "nodes-to-rds" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.allow-rds.id}"
  source_security_group_id = "${aws_security_group.nodes.id}"
  from_port                = 1521
  to_port                  = 1521
  protocol                 = "tcp"
}

resource "aws_security_group" "allow-rds" {
  name        = "allow-rds"
  vpc_id      = "${aws_vpc.vpc.id}"
  description = "Security Group for RDS"

  tags = {
    Name = "allow-rds.${var.cluster_name}.${var.public_domain}"
  }
}

resource "aws_security_group" "master-efs" {
  name        = "master-efs.${var.cluster_name}.${var.public_domain}"
  vpc_id      = "${aws_vpc.vpc.id}"
  description = "Security Group for EFS to master"

  tags = {
    Name = "master-efs.${var.cluster_name}.${var.public_domain}"
  }
}

resource "aws_security_group" "efs-controller" {
  name        = "efs-controller.${var.cluster_name}.${var.public_domain}"
  vpc_id      = "${aws_vpc.vpc.id}"
  description = "Security Group for EFS Controller"

  tags = {
    Name = "efs-controller.${var.cluster_name}.${var.public_domain}"
  }
}

resource "aws_security_group" "bastion" {
  name        = "bastion.${var.cluster_name}.${var.public_domain}"
  vpc_id      = "${aws_vpc.vpc.id}"
  description = "Security group for bastion"

  tags = {
    Name = "bastion.${var.cluster_name}.${var.public_domain}"
  }
}

resource "aws_security_group" "masters" {
  name        = "masters.${var.cluster_name}.${var.public_domain}"
  vpc_id      = "${aws_vpc.vpc.id}"
  description = "Security group for masters"

  tags = {
    Name = "masters.${var.cluster_name}.${var.public_domain}"
  }
}

resource "aws_security_group" "nodes" {
  name        = "nodes.${var.cluster_name}.${var.public_domain}"
  vpc_id      = "${aws_vpc.vpc.id}"
  description = "Security group for nodes"

  tags = {
    Name = "nodes.${var.cluster_name}.${var.public_domain}"
  }
}

resource "aws_route_table_association" "nodes-private-a" {
  subnet_id      = "${aws_subnet.nodes-private-zone-a.id}"
  route_table_id = "${aws_route_table.private-zone-a.id}"
}

resource "aws_route_table_association" "nodes-private-b" {
  subnet_id      = "${aws_subnet.nodes-private-zone-b.id}"
  route_table_id = "${aws_route_table.private-zone-b.id}"
}

resource "aws_route_table_association" "nodes-private-c" {
  subnet_id      = "${aws_subnet.nodes-private-zone-c.id}"
  route_table_id = "${aws_route_table.private-zone-c.id}"
}

resource "aws_route_table_association" "master-private-a" {
  subnet_id      = "${aws_subnet.master-private-zone-a.id}"
  route_table_id = "${aws_route_table.private-zone-a.id}"
}

resource "aws_route_table_association" "master-private-b" {
  subnet_id      = "${aws_subnet.master-private-zone-b.id}"
  route_table_id = "${aws_route_table.private-zone-b.id}"
}

resource "aws_route_table_association" "master-private-c" {
  subnet_id      = "${aws_subnet.master-private-zone-c.id}"
  route_table_id = "${aws_route_table.private-zone-c.id}"
}

resource "aws_route_table_association" "public-zone-a" {
  subnet_id      = "${aws_subnet.public-zone-a.id}"
  route_table_id = "${aws_route_table.public.id}"
}

resource "aws_route_table_association" "public-zone-b" {
  subnet_id      = "${aws_subnet.public-zone-b.id}"
  route_table_id = "${aws_route_table.public.id}"
}

resource "aws_route_table_association" "plubic-zone-c" {
  subnet_id      = "${aws_subnet.public-zone-c.id}"
  route_table_id = "${aws_route_table.public.id}"
}

resource "aws_route" "0-0-0-0--0" {
  route_table_id         = "${aws_route_table.public.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.gateway.id}"
}

resource "aws_route" "private-a-0-0-0-0--0" {
  route_table_id         = "${aws_route_table.private-zone-a.id}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${aws_nat_gateway.nat-gateway-zone-a.id}"
}

resource "aws_route" "private-b-0-0-0-0--0" {
  route_table_id         = "${aws_route_table.private-zone-b.id}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${aws_nat_gateway.nat-gateway-zone-b.id}"
}

resource "aws_route" "private-c-0-0-0-0--0" {
  route_table_id         = "${aws_route_table.private-zone-c.id}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${aws_nat_gateway.nat-gateway-zone-c.id}"
}

resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags {
    Name = "public.${var.cluster_name}.${var.public_domain}"
  }
}

resource "aws_route_table" "private-zone-a" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    Name = "private-zone-a.${var.cluster_name}.${var.public_domain}"
  }
}

resource "aws_route_table" "private-zone-b" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    Name = "private-zone-b.${var.cluster_name}.${var.public_domain}"
  }
}

resource "aws_route_table" "private-zone-c" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    Name = "private-zone-c.${var.cluster_name}.${var.public_domain}"
  }
}

resource "aws_eip" "nat-ip-a" {
  vpc = true
}

resource "aws_eip" "nat-ip-b" {
  vpc = true
}

resource "aws_eip" "nat-ip-c" {
  vpc = true
}

resource "aws_nat_gateway" "nat-gateway-zone-a" {
  allocation_id = "${aws_eip.nat-ip-a.id}"
  subnet_id     = "${aws_subnet.public-zone-a.id}"
}

resource "aws_nat_gateway" "nat-gateway-zone-b" {
  allocation_id = "${aws_eip.nat-ip-b.id}"
  subnet_id     = "${aws_subnet.public-zone-b.id}"
}

resource "aws_nat_gateway" "nat-gateway-zone-c" {
  allocation_id = "${aws_eip.nat-ip-c.id}"
  subnet_id     = "${aws_subnet.public-zone-c.id}"
}

resource "aws_internet_gateway" "gateway" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags {
    Name = "${var.cluster_name}.${var.public_domain}"
  }
}

resource "aws_subnet" "master-private-zone-a" {
  vpc_id            = "${aws_vpc.vpc.id}"
  cidr_block        = "172.20.100.0/23"
  availability_zone = "${var.region}a"

  tags = {
    Name       = "master.${var.region}a.${var.cluster_name}.${var.public_domain}"
    SubnetType = "private"
    Role       = "master"
  }
}

resource "aws_subnet" "master-private-zone-b" {
  vpc_id            = "${aws_vpc.vpc.id}"
  cidr_block        = "172.20.102.0/23"
  availability_zone = "${var.region}b"

  tags = {
    Name       = "master.${var.region}b.${var.cluster_name}.${var.public_domain}"
    SubnetType = "private"
    Role       = "master"
  }
}

resource "aws_subnet" "master-private-zone-c" {
  vpc_id            = "${aws_vpc.vpc.id}"
  cidr_block        = "172.20.104.0/23"
  availability_zone = "${var.region}c"

  tags = {
    Name       = "master.${var.region}c.${var.cluster_name}.${var.public_domain}"
    SubnetType = "private"
    Role       = "master"
  }
}

# For nodes we created a network with mask of 22 with 1022 hosts avaiable, because for this
# project (TAF) we don't need more than 60 nodes per cluster. 
resource "aws_subnet" "nodes-private-zone-a" {
  vpc_id            = "${aws_vpc.vpc.id}"
  cidr_block        = "172.20.48.0/20"
  availability_zone = "${var.region}a"

  tags = {
    Name       = "nodes.${var.region}a.${var.cluster_name}.${var.public_domain}"
    SubnetType = "private"
    Role       = "nodes"
  }
}

resource "aws_subnet" "nodes-private-zone-b" {
  vpc_id            = "${aws_vpc.vpc.id}"
  cidr_block        = "172.20.32.0/20"
  availability_zone = "${var.region}b"

  tags = {
    Name       = "nodes.${var.region}b.${var.cluster_name}.${var.public_domain}"
    SubnetType = "private"
    Role       = "nodes"
  }
}

resource "aws_subnet" "nodes-private-zone-c" {
  vpc_id            = "${aws_vpc.vpc.id}"
  cidr_block        = "172.20.16.0/20"
  availability_zone = "${var.region}c"

  tags = {
    Name       = "nodes.${var.region}c.${var.cluster_name}.${var.public_domain}"
    SubnetType = "private"
    Role       = "nodes"
  }
}

resource "aws_subnet" "public-zone-a" {
  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "172.20.0.0/22"
  availability_zone       = "${var.region}a"
  map_public_ip_on_launch = true

  tags = "${merge(
    local.common_tags,
    map(
      "Name", "public.${var.region}c.${var.cluster_name}.${var.public_domain}",
      "SubnetType" , "Public"
      )
  )}"
}

resource "aws_subnet" "public-zone-b" {
  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "172.20.4.0/22"
  availability_zone       = "${var.region}b"
  map_public_ip_on_launch = true

  tags = "${merge(
    local.common_tags,
    map(
      "Name", "public.${var.region}c.${var.cluster_name}.${var.public_domain}",
      "SubnetType" , "Public"
      )
  )}"
}

resource "aws_subnet" "public-zone-c" {
  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "172.20.8.0/22"
  availability_zone       = "${var.region}c"
  map_public_ip_on_launch = true

  tags = "${merge(
    local.common_tags,
    map(
      "Name", "public.${var.region}c.${var.cluster_name}.${var.public_domain}",
      "SubnetType" , "Public"
      )
  )}"
}

resource "aws_vpc_dhcp_options" "dhcp-options" {
  domain_name         = "${var.region}.compute.internal"
  domain_name_servers = ["AmazonProvidedDNS"]

  tags = {
    Name = "${var.cluster_name}.${var.public_domain}"
  }
}

resource "aws_vpc_dhcp_options_association" "dhcp_options_association" {
  vpc_id          = "${aws_vpc.vpc.id}"
  dhcp_options_id = "${aws_vpc_dhcp_options.dhcp-options.id}"
}

resource "aws_vpc" "vpc" {
  cidr_block           = "172.20.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "${var.cluster_name}.${var.public_domain}"
  }
}

terraform = {
  required_version = ">= 0.11.6"
}
