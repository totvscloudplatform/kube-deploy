#!/bin/bash

set -x

KUBEADM_CONFIG_PATH=/etc/kubernetes/kubeadm.conf
KUBERNETES_VERSION=${k8s_version}
ETCD_DATA_DIR=var/lib/etcd
CERTIFICATES_DIRS=etc/kubernetes/pki
EFS_DNS=${efs_dns}
KUBEADM_TOKEN=${kubeadm_token}
OIDC_ISSUER_URL=https://taf.auth0.com/
OIDC_CLIENT_ID=a92FQnDpTCpAzJgrXvqbKqFohNsTXdcT
GROUPS_CLAIM=http://smartfiscal.info/groups

# Prepare local folders and mount EFS
sudo mkdir -p /root-efs /$ETCD_DATA_DIR /$CERTIFICATES_DIRS
while true; do
  echo "waiting efs response..."
  host $EFS_DNS > /dev/null
  [[ $(echo $?) -eq 0 ]] && break
  sleep 5
done
sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=5,_netdev $EFS_DNS:/ /root-efs
# Create the root file system paths into EFS
sudo mkdir -p /root-efs/$ETCD_DATA_DIR /root-efs/$CERTIFICATES_DIRS

echo "$EFS_DNS:/$CERTIFICATES_DIRS /$CERTIFICATES_DIRS nfs4 nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=5,_netdev" >> /etc/fstab
echo "$EFS_DNS:/$ETCD_DATA_DIR /$ETCD_DATA_DIR nfs4 nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=5,_netdev" >> /etc/fstab

# mount -a
sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=5,_netdev $EFS_DNS:/$CERTIFICATES_DIRS /$CERTIFICATES_DIRS
sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=5,_netdev $EFS_DNS:/$ETCD_DATA_DIR /$ETCD_DATA_DIR

LOCAL_IP=$(curl -s http://169.254.169.254/latest/meta-data/local-ipv4)

ADVERTISE_ADDRESS=$LOCAL_IP
NODENAME=$(hostname -f)
INTERNAL_MASTER_ADDRESS=${internal_master_address}
EXTERNAL_MASTER_ADDRESS=${external_master_address}

# This is necessary because master doesn't connect to Network Load Balancer own instance.
echo $LOCAL_IP $INTERNAL_MASTER_ADDRESS >> /etc/hosts

sudo /bin/bash -c "cat <<EOF > $KUBEADM_CONFIG_PATH
apiVersion: kubeadm.k8s.io/v1alpha1
kind: MasterConfiguration
api:
  controlPlaneEndpoint: $INTERNAL_MASTER_ADDRESS
  advertiseAddress: $ADVERTISE_ADDRESS
  bindPort: 6443
authorizationModes:
- Node
- RBAC
cloudProvider: aws
certificatesDir: /$CERTIFICATES_DIRS
apiServerCertSANs:
  - $INTERNAL_MASTER_ADDRESS
  - $EXTERNAL_MASTER_ADDRESS
apiServerExtraArgs:
  insecure-port: '8080'
  oidc-client-id: $OIDC_CLIENT_ID
  oidc-groups-claim: $GROUPS_CLAIM
  oidc-issuer-url: $OIDC_ISSUER_URL
  oidc-username-claim: email
controllerManagerExtraArgs:
  node-monitor-grace-period: 300s
  node-eviction-rate: '.00333'
  secondary-node-eviction-rate: '.00333'
  configure-cloud-routes: 'false'
  master: https://$ADVERTISE_ADDRESS:6443
  address: 0.0.0.0
schedulerExtraArgs:
  address: 0.0.0.0
  master: https://$ADVERTISE_ADDRESS:6443
etcd:
  endpoints: []
  caFile: ""
  certFile: ""
  keyFile: ""
  dataDir: /$ETCD_DATA_DIR
imageRepository: gcr.io/google_containers
kubernetesVersion: $KUBERNETES_VERSION
networking:
  dnsDomain: cluster.local
  podSubnet: 192.168.0.0/16
  serviceSubnet: 10.96.0.0/12
nodeName: $NODENAME
token: $KUBEADM_TOKEN
tokenTTL: 0s # never expires
unifiedControlPlaneImage: ""
featureGates:
  CoreDNS: true
EOF"

if [ -z "$(ls -A /etc/kubernetes/pki)" ]; then
  sudo kubeadm init --config $KUBEADM_CONFIG_PATH
  mkdir -p $HOME/.kube
  sudo cp /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

  # This command is necessary to ensure that all kubelets get a valid DNS name instead of the ip address
  # of the master, in the future we must change to section api: `controlPlaneEndpoint`
  # PR Ref: https://github.com/kubernetes/kubernetes/pull/59288
  # Usage details: https://github.com/heptiolabs/wardroom/pull/41
  # kubectl get cm -n kube-public cluster-info -o yaml  |sed "s|server:.*|server: https://$INTERNAL_MASTER_ADDRESS:6443|g" | kubectl replace -f -

  kubectl create -f https://docs.projectcalico.org/v3.0/getting-started/kubernetes/installation/hosted/kubeadm/1.7/calico.yaml

  # Install Kong
  kubectl create ns kong
  kubectl create secret generic -n kong kong-credentials \
    --from-literal=password=$(date +%s | sha1sum | base64 | head -c 32 ; echo) \
    --from-literal=username=kong
  kubectl apply -f https://bitbucket.org/totvscloudplatform/kube-deploy/raw/master/terraform/aws/kubernetes/manifests/kong/rbac.yml
  kubectl apply -f https://bitbucket.org/totvscloudplatform/kube-deploy/raw/master/terraform/aws/kubernetes/manifests/kong/crd.yml
  kubectl apply -f https://bitbucket.org/totvscloudplatform/kube-deploy/raw/master/terraform/aws/kubernetes/manifests/kong/kong.yml

  # Install taf cluster roles
  kubectl apply -f https://bitbucket.org/totvscloudplatform/kube-deploy/raw/master/terraform/aws/kubernetes/manifests/clusterroles/taf:admin.yml
  kubectl apply -f https://bitbucket.org/totvscloudplatform/kube-deploy/raw/master/terraform/aws/kubernetes/manifests/clusterroles/taf:operator.yml
  kubectl apply -f https://bitbucket.org/totvscloudplatform/kube-deploy/raw/master/terraform/aws/kubernetes/manifests/clusterroles/taf:viewer.yml

  # Install Storage Class
  kubectl apply -f https://bitbucket.org/totvscloudplatform/kube-deploy/raw/master/terraform/aws/kubernetes/manifests/storage-class.yml

  # Install EFS Controller
  kubectl apply -f https://bitbucket.org/totvscloudplatform/kube-deploy/raw/master/terraform/aws/kubernetes/manifests/efs-controller/rbac.yml
  kubectl apply -f https://bitbucket.org/totvscloudplatform/kube-deploy/raw/master/terraform/aws/kubernetes/manifests/efs-controller/storage-class.yml

	# Create taf-system:kraken-provisioner ServiceAccount
	kubectl apply -f https://bitbucket.org/totvscloudplatform/kube-deploy/raw/master/terraform/aws/kubernetes/manifests/service-accounts/taf-system_kraken-provisioner/00-namespace.yaml
	kubectl apply -f https://bitbucket.org/totvscloudplatform/kube-deploy/raw/master/terraform/aws/kubernetes/manifests/service-accounts/taf-system_kraken-provisioner/01-serviceaccount.yaml
	kubectl apply -f https://bitbucket.org/totvscloudplatform/kube-deploy/raw/master/terraform/aws/kubernetes/manifests/service-accounts/taf-system_kraken-provisioner/10-clusterrole.yaml
	kubectl apply -f https://bitbucket.org/totvscloudplatform/kube-deploy/raw/master/terraform/aws/kubernetes/manifests/service-accounts/taf-system_kraken-provisioner/11-role_portforward.yaml
	kubectl apply -f https://bitbucket.org/totvscloudplatform/kube-deploy/raw/master/terraform/aws/kubernetes/manifests/service-accounts/taf-system_kraken-provisioner/20-clusterrolebinding.yaml
	kubectl apply -f https://bitbucket.org/totvscloudplatform/kube-deploy/raw/master/terraform/aws/kubernetes/manifests/service-accounts/taf-system_kraken-provisioner/21-rolebinding_portforward.yaml

  CONTROLLER_EFS01_ID=${controller_efs01_id}
  CONTROLLER_EFS01_ADDRESS=${controller_efs01_address}

/bin/bash -c "cat <<EOF
apiVersion: v1
kind: ConfigMap
metadata:
  name: efs01-provisioner
data:
  file.system.id: $CONTROLLER_EFS01_ID
  aws.region: us-east-2
  provisioner.name: efs01-volume/aws-efs
---
kind: Deployment
apiVersion: extensions/v1beta1
metadata:
  name: efs01-provisioner
spec:
  replicas: 1
  strategy:
    type: Recreate 
  template:
    metadata:
      labels:
        app: efs01-provisioner
    spec:
      serviceAccountName: efs01-provisioner
      containers:
        - name: efs-provisioner
          image: quay.io/external_storage/efs-provisioner:latest
          env:
            - name: FILE_SYSTEM_ID
              valueFrom:
                configMapKeyRef:
                  name: efs01-provisioner
                  key: file.system.id
            - name: AWS_REGION
              valueFrom:
                configMapKeyRef:
                  name: efs01-provisioner
                  key: aws.region
            - name: PROVISIONER_NAME
              valueFrom:
                configMapKeyRef:
                  name: efs01-provisioner
                  key: provisioner.name
          volumeMounts:
            - name: pv-volume
              mountPath: /persistentvolumes
      volumes:
        - name: pv-volume
          nfs:
            server: $CONTROLLER_EFS01_ADDRESS
            path: /
EOF" | kubectl apply -f -

  sudo /bin/bash -c 'curl https://storage.googleapis.com/kubernetes-helm/helm-v2.8.2-linux-amd64.tar.gz | tar -O -xzvf - linux-amd64/helm > /usr/local/bin/helm'
  sudo chmod +x /usr/local/bin/helm

  # https://github.com/kubernetes/helm/blob/master/docs/rbac.md
  kubectl -n kube-system create sa tiller
  kubectl create clusterrolebinding tiller --clusterrole cluster-admin --serviceaccount=kube-system:tiller
  helm init --service-account tiller
  tries="0"
  while [ $tries -lt 60 ]; do
    echo -n "Trying for $tries/60 - "
    helm list && break;
    sleep 5
    tries=$[$tries+1]
  done

  # Install monitoring and addons stuff
  helm init -c
  helm repo add coreos https://s3-eu-west-1.amazonaws.com/coreos-charts/stable/
  helm install coreos/prometheus-operator --name prom-operator --namespace monitoring
  helm install coreos/kube-prometheus --name kube-prom --set rbacEnable=true --namespace monitoring
  helm install stable/heapster --name mon --set rbac.create=true --namespace kube-system
  helm install stable/kubernetes-dashboard --name dash --namespace kube-system

  # Fix monitoring control plane components
  # kube-scheduler fix
  kubectl patch svc kube-prom-exporter-kube-scheduler -n kube-system --type='json' \
    -p='[{"op": "remove", "path": "/spec/selector/k8s-app"}]' || true
  kubectl patch svc kube-prom-exporter-kube-scheduler -n kube-system \
    -p '{"spec":{"selector": {"component": "kube-scheduler"}}}' || true
  
  # controller-manager fix
  kubectl patch svc kube-prom-exporter-kube-controller-manager -n kube-system --type='json' \
    -p='[{"op": "remove", "path": "/spec/selector/k8s-app"}]' || true
  kubectl patch svc kube-prom-exporter-kube-controller-manager -n kube-system \
    -p '{"spec":{"selector": {"component": "kube-controller-manager"}}}' || true

  # etcd fix
  # TODO: need to bind etcd :4001 to 0.0.0.0
  kubectl patch svc kube-prom-exporter-kube-etcd -n kube-system --type='json' \
    -p='[{"op": "remove", "path": "/spec/selector/k8s-app"}]' || true
  kubectl patch svc kube-prom-exporter-kube-etcd -n kube-system \
    -p '{"spec":{"selector": {"component": "etcd"}}}' || true

  exit 0
fi
  
  
echo "PKI folder isn't empty, it will not initialize a new cluster!"
