#!/bin/bash

set -x

# not needed on nodes!
sudo systemctl stop kubeadm-recover
sudo systemctl disable kubeadm-recover

sudo kubeadm join --token ${kubeadm_token} --discovery-token-unsafe-skip-ca-verification ${internal_master_address}:6443
