#!/bin/bash

: "${AWS_ASSUME_ROLE_ARN:?Need to set environment AWS_ASSUME_ROLE_ARN.}"
: "${AWS_REGION:?Need to set environment AWS_REGION.}"
: "${KERNEL_VERSION:?Need to set environment KERNEL_VERSION.}"
: "${K8S_REPO_VERSION:?Need to set environment K8S_REPO_VERSION.}"
: "${K8S_VERSION:?Need to set environment K8S_VERSION.}"

echo '---> git clone'
git clone https://bitbucket.org/totvscloudplatform/kube-deploy.git

echo '---> aws assume role'

eval $(aws sts assume-role --role-arn "$AWS_ASSUME_ROLE_ARN" --role-session-name "k8s-bootstrap" |  \
    jq -r '.Credentials |"AWS_ACCESS_KEY_ID="+.AccessKeyId+" "+"AWS_SECRET_ACCESS_KEY="+.SecretAccessKey+" "+"AWS_SESSION_TOKEN="+.SessionToken')

echo '---> creating image'
(cd kube-deploy/kubeadm/packer/ &&
packer build \
  -var "aws_access_key=$AWS_REGION" \
  -var "aws_access_key=$AWS_ACCESS_KEY_ID" \
  -var "aws_secret_key=$AWS_SECRET_ACCESS_KEY" \
  -var "aws_session_token=$AWS_SESSION_TOKEN" \
  -var "kernel_version=$KERNEL_VERSION" \
  -var "k8s_repo_version=$K8S_REPO_VERSION" \
  -var "k8s_version=$K8S_VERSION" \
k8s.json)

echo '---> image created!'

echo '---> cleaning up execution'
unset KERNEL_VERSION
unset K8S_REPO_VERSION
unset K8S_VERSION
unset AWS_REGION
unset AWS_ASSUME_ROLE_ARN

rm -rf .packer.d