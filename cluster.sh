#!/bin/bash

: "${TF_VAR_region:?Need to set environment TF_VAR_region.}"
: "${TF_VAR_public_domain:?Need to set environment TF_VAR_public_domain.}"
: "${TF_VAR_cluster_name:?Need to set environment TF_VAR_cluster_name.}"
: "${TF_VAR_k8s_version:?Need to set environment TF_VAR_k8s_version.}"
: "${TF_VAR_assume_role_arn:?Need to set environment TF_VAR_assume_role_arn.}"
: "${ACTION:?Need to set environment ACTION.}"

echo '---> git clone'
git clone https://bitbucket.org/totvscloudplatform/kube-deploy.git

if  aws s3 ls k8s.$TF_VAR_public_domain > /dev/null; then
  echo 'bucket already created'
else
  echo '---> creating terraform backend bucket'
  aws s3api create-bucket --bucket "k8s.$TF_VAR_public_domain" --region "$TF_VAR_region" \
      --create-bucket-configuration LocationConstraint="$TF_VAR_region"
  aws s3api put-bucket-versioning --bucket "k8s.$TF_VAR_public_domain" --versioning-configuration Status=Enabled
fi

echo '---> creating cluster...'
(cd kube-deploy/terraform/aws/kubernetes/ &&
cat > remote-state.tf << EOF
terraform {
  backend "s3" {
    bucket  = "k8s.$TF_VAR_public_domain"
    key     = "tf-state-$TF_VAR_cluster_name/terraform.tfstate"
    region  = "$TF_VAR_region"
  }
}
EOF

terraform init
# https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-init/#automating-kubeadm
KUBEADM_TOKEN=$(echo $(openssl rand -hex 3).$(openssl rand -hex 8)) &&

terraform $ACTION -auto-approve -var kubeadm_token=$KUBEADM_TOKEN)

echo "---> cluster created! Api Server: https://api.$TF_VAR_cluster_name.$TF_VAR_public_domain"

echo '---> cleaning up execution'
unset TF_VAR_region
unset TF_VAR_public_domain
unset TF_VAR_cluster_name
unset TF_VAR_k8s_version
unset TF_VAR_zone
unset TF_VAR_assume_role_arn

rm -rf kube-deploy .terraform.d