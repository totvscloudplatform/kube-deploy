# Build with Packer

Assuming that you have valid credentials on aws to run packer (~/.aws/credentials)

```
packer build \
  -var "kernel_version=$KERNEL_VERSION" \
  -var "k8s_repo_version=$K8S_REPO_VERSION" \
  -var "k8s_version=$K8S_VERSION" \
  k8s.json
```

> **K8S_REPO_VERSION:** must follow semantics from [Kubernetes RHEL Repository.](https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64/repodata)
>
> To acquire a list of available versions using `yum`
> ```
> sudo yum list available <kubeadm|kubelet|kubectl> --showduplicates
> ```

This will create a custom AMI on your AWS account.

More Info: https://www.packer.io/intro/getting-started/build-image.html#your-first-image
```