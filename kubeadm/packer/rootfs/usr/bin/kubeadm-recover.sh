#!/usr/bin/env bash

set -x

# The pki folders doesn't contain any certificate, don't try to recover!
if [ -z "$(ls -A /etc/kubernetes/pki)" ]; then
    echo "PKI folder is empty, it will not try to recover the control plane!"
    exit 1
fi

kubeadm alpha phase kubeconfig all --config /etc/kubernetes/kubeadm.conf
systemctl restart kubelet
kubeadm alpha phase etcd local --config /etc/kubernetes/kubeadm.conf
kubeadm alpha phase controlplane all --config /etc/kubernetes/kubeadm.conf
kubeadm alpha phase mark-master --config /etc/kubernetes/kubeadm.conf
