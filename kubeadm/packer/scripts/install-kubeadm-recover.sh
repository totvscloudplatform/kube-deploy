#!/usr/bin/env bash

sudo chmod +x /home/centos/kubeadm-recover.service
sudo chmod +x /home/centos/kubeadm-recover.sh

sudo mv /home/centos/kubeadm-recover.service /etc/systemd/system/kubeadm-recover.service
sudo mv /home/centos/kubeadm-recover.sh /usr/bin/kubeadm-recover.sh
sudo systemctl enable kubeadm-recover
