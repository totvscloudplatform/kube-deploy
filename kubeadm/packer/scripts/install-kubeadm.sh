#!/bin/bash

set -eo pipefail

# Disabling SELinux by running setenforce 0 is required to allow 
# containers to access the host filesystem, which is required by
# pod networks for example. You have to do this until
# SELinux support is improved in the kubelet.
sudo setenforce 0

# Disable it to persist on reboots
sudo sed -i 's/SELINUX=enforcing/SELINUX=permissive/g' /etc/selinux/config

sudo systemctl stop postfix
sudo systemctl disable postfix

BASEARCH=$(uname -p)
sudo bash -c "cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-$BASEARCH
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF"

sudo yum update -y

# Some users on RHEL/CentOS 7 have reported issues with traffic being routed incorrectly due to iptables being bypassed.
# You should ensure net.bridge.bridge-nf-call-iptables is set to 1 in your sysctl config
sudo bash -c "cat <<EOF >  /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF"

sudo sysctl --system


echo "=====> INSTALLING DOCKER 1.12.6"
# reference https://kubernetes.io/docs/setup/independent/install-kubeadm/#installing-docker
sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-selinux \
                  docker-engine-selinux \
                  docker-engine

sudo yum install -y yum-utils \
                    device-mapper-persistent-data \
                    lvm2
sudo yum-config-manager --add-repo https://yum.dockerproject.org/repo/main/centos/7
sudo yum -y update
sudo yum -y --nogpgcheck install docker-engine-1.12.6-1.el7.centos.x86_64


# change storage driver https://docs.docker.com/storage/storagedriver/overlayfs-driver/#configure-docker-with-the-overlay-or-overlay2-storage-driver
sudo mkdir -p /etc/docker
sudo /bin/bash -c 'cat <<EOF > /etc/docker/daemon.json
{
    "storage-driver": "overlay"
}
EOF'

echo "=====> INSTALLING KUBERNETES BINARIES: $KUBERNETES_VERSION"
sudo yum install -y \
    conntrack-tools \
    kubelet-$KUBERNETES_VERSION \
    kubeadm-$KUBERNETES_VERSION \
    kubectl-$KUBERNETES_VERSION \
    

echo "=====> INSTALLING HOST UTILS"
# https://github.com/draios/sysdig/wiki/How-to-Install-Sysdig-for-Linux

sudo rpm --import https://s3.amazonaws.com/download.draios.com/DRAIOS-GPG-KEY.public  
sudo curl -s -o /etc/yum.repos.d/draios.repo https://s3.amazonaws.com/download.draios.com/stable/rpm/draios.repo
sudo rpm -i https://mirror.us.leaseweb.net/epel/6/i386/epel-release-6-8.noarch.rpm

# Kernel headers
sudo yum install -y kernel-devel-$(uname -r)
sudo yum install -y \
    sysdig \
    sysstat \
    telnet \
    bind-utils \
    screen \
    vim

# Remove postfix
sudo yum remove -y postfix

sudo systemctl enable docker
sudo systemctl start docker

KUBEADM_SYSTEMD_CONF=/etc/systemd/system/kubelet.service.d/10-kubeadm.conf
CGROUP_DRIVER=$(sudo docker info | awk '/Cgroup Driver/{print $3}')
sudo sed -i "s/cgroup-driver=systemd/cgroup-driver=$CGROUP_DRIVER/g" $KUBEADM_SYSTEMD_CONF

# https://github.com/coreos/prometheus-operator/blob/master/contrib/kube-prometheus/docs/kube-prometheus-on-kubeadm.md
sudo sed -e "/cadvisor-port=0/d" -i "$KUBEADM_SYSTEMD_CONF"
if ! sudo grep -q "authentication-token-webhook=true" "$KUBEADM_SYSTEMD_CONF"; then
  sudo sed -e "s/--authorization-mode=Webhook/--authentication-token-webhook=true --authorization-mode=Webhook/" -i "$KUBEADM_SYSTEMD_CONF"
fi

sudo systemctl daemon-reload
sudo systemctl restart kubelet

# --cloud-provider option must be present on master and nodes
sudo bash -c "cat <<EOF > /etc/systemd/system/kubelet.service.d/20-extra-args.conf 
[Service]
Environment="KUBELET_EXTRA_ARGS=--cloud-provider=aws"
EOF"

sudo systemctl daemon-reload
sudo systemctl enable kubelet

echo "=====> DONE!"