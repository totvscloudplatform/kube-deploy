# A repo for cluster deployment automation [DEPRECATED]

new repo: https://github.com/cloud104/kube-deploy


> to create k8s image run:
```bash
export AWS_REGION="us-east-2"
export AWS_ASSUME_ROLE_ARN="arn:aws:iam::351702516288:role/BootStrap-CrossAccount"
export KERNEL_VERSION="3.10"
export K8S_REPO_VERSION="1.10.1-0"
export K8S_VERSION="v1.10.1"

bash <(curl -s https://bitbucket.org/totvscloudplatform/kube-deploy/raw/master/packer.sh)
```

> to create cluster run:
```bash
export TF_VAR_region="us-east-2"
export TF_VAR_public_domain="smartfiscal.info"
export TF_VAR_cluster_name="ke02"
export TF_VAR_k8s_version="v1.10.1"
export TF_VAR_zone="b"
export TF_VAR_assume_role_arn="arn:aws:iam::351702516288:role/BootStrap-CrossAccount"
export TF_VAR_public_key="public_key_for_ssh"
export ACTION=apply

bash <(curl -s https://bitbucket.org/totvscloudplatform/kube-deploy/raw/master/cluster.sh)
```